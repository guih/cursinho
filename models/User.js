var mongoose = require('mongoose');  
var Owner = new mongoose.Schema({  
	'name' 			: { type: String },
	'number' 		: { type: Number },
	'caption' 		: { type: String },	
	'level'			: { type: String, default: 1 },
	'token'			: { type: String },
	'cpf'			: { type: String },
	'cnpj'			: { type: String },
	'creci'			: { type: String },
	'password'		: { type: String },
	'email'			: { type: String },
	'phone'			: { type: String },
	'mobilePhone'	: { type: String },
	'avatar'		: { type: String, default: 'https://lh3.googleusercontent.com/-B4XQheUg1Gw/AAAAAAAAAAI/AAAAAAAABGk/0zJNOvxIPrA/s28-c-k-no/photo.jpg' },
	'timestamp'		: { type: String, default: +new Date() },
	'houses'		: { type: Array, default: [] }
});
module.exports = mongoose.model('User', Owner);