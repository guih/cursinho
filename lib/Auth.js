var jwt = require('jsonwebtoken');

var Auth = {
	sign: function(user){
		delete user.token;
		user.lastLogin = (+new Date());

		return jwt.sign(JSON.stringify(user), '123'); 
	},
	verify: function(req, res, next) {

		var token = req.body.token || req.query.token || req.headers['x-access-token'];


		if (token) {
			jwt.verify(token, '123', function(err, decoded) {      
				if (err) {
					return res.status(401).json({'error': 'InvalidSession', msg: 'Invalid acess token!'});    
				} else {
					req.user = decoded;    

					next();
				}
			});

			return;
		}

		
		return res.status(403).json({'error': 'NoToken', msg: 'No token supplied!'});    
		
		
	}
};


module.exports = Auth;