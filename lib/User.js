var mongoose    = require('mongoose');
var db 			= require('../models/db');
var UserModel 	= require('../models/User');
var Crypto 		= require('../lib/Crypto');


var User = {
	create: function(data, fn){
		data.password = Crypto.hash( data.password );

		UserModel.create( data, function(err, user){
			if( err ) { console.log('Error while creating user!'); return; }

			fn( user );

		} );
	},
	find: function(id, fn){
		UserModel.find({_id: id}, function(err, user){
			if( err ) { console.log('Error while fetching user!'); return; }


			fn( null, user[0] );
		});
	}, 

	findAll: function (fn) {	
		
		UserModel.find( {}, function(err, users){
			if( err ) { console.log('Error while fetching users!'); return; }


			fn( users );	
			
		});
	},
	delete: function(id, fn){

		UserModel.remove({_id: id}, function(err, user){
			console.log(arguments, id);

			fn( null, {msg : 'deleted!'} );
		});

	},
	update: function(id, data, fn){
		
		if( data.password )
			data.password = Crypto.hash( data.password );

		UserModel.findOneAndUpdate( {_id : id}, data, function(err, newUser){
			if( err ) { console.log('Error while updating user!'); return; }

			fn( null, newUser );
		});
	},
	login: function(email, pass, fn){
		UserModel.find({ email: email }, function(err, user){
			var user = user[0];

			if( err || user.length < 1 ) { console.log('Error while fetching user!'); return; }


			if( user.password == Crypto.hash(pass) ){
				fn(null, user);
				return;
			}

			fn({error: 'InvalidLogin', msg: 'Invalid login or password!'});


		});
	},
	findHouses: function(id, fn){
		this.find( id, function(err, res){
			if( err ) 
				return fn( {error: 'InternalError', msg: 'Can\'t proceed with this request'} );
			if( res == null || !res ) 
				return fn( {error: 'InternalError', msg: 'Can\'t find user'} );


			return fn( null, res.houses );
			

		} );
	},
	filters: {
		search: function(q, fn){
			q = q.split(' ');
			query = [];
			for (var i = q.length - 1; i >= 0; i--) {
				query.push(new RegExp([q[i]].join(""), "i"))
			};
		
			console.log(query)
			UserModel.find({
			    "houses": {
			        $all: [{
			            $elemMatch: {
			                "title": query,
			                
			            }
			        }, ]
			    }			    
			}).exec(function(err, docs) { 				
				if(! docs ) return fn( {error: 'NotFound', msg: 'Document not found!'} )
				docs = docs.map(function(doc){
					return doc.houses;
				})

				fn(null, docs);				
			});
		}
	}
};


module.exports = User;