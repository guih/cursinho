var express = require('express');
var router 	= express.Router();
var User 	= require('../lib/User');
var Auth 	= require('../lib/Auth');

router.get('/', function(req, res, next) {
   	User.findAll(function(users){
   		res.status(200).json(users);
   	});
});

router.post('/', function(req, res, next) {
  var user = req.body;
  User.create( user, function(response){
      res.status(200).json( response );
  } );  
});


router.get('/:id', function(req, res, next) {
	var id = req.params.id;

   	User.find(id, function(err, user){
   		res.status(200).json(user);
   	});
});

router.get('/:id/houses', function(req, res, next) {
	var id = req.params.id;

   	User.findHouses(id, function(err, houses){
   		res.status(200).json(houses);
   	});
});




router.post('/update', function(req, res, next) {
	var user = req.body;
	var id = req.body._id;
	delete user._id;

	User.update( id, user, function(err, response){
		console.log(response)
  		res.status(200).json( response );
	} );
});

router.post('/delete', function(req, res, next) {
  User.delete(req.body.id, function(err, response){
  	res.status(200).json(response);
  });
});

// router.post('/profile', Auth.verify, function(req, res, next){
// 	res.json(req.user);
// });


module.exports = router;
