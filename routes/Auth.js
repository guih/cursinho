var express = require('express');
var User 	= require('../lib/User');
var Auth 	= require('../lib/Auth');
var router 	= express.Router();

router.post('/', function(req, res, next) {  	

	var user = req.body.email;
	var pass = req.body.password;


  	User.login(user, pass, function(err, user){

  		if( err ) return res.status(401).json(err);


  		var user = user.toJSON();

  		var token = Auth.sign( user );

  		User.update( user._id, {"token": token}, function(err, userRes){
  			if( err )  return res.status(500).json(err);

	  		delete user.password;
	  		delete user.__v;

	  		res.status(200).json({ acessToken: userRes.token });
  		} )
  	});
});	

module.exports = router;
