var express = require('express');
var User 	= require('../lib/User');
var Crypto 	= require('../lib/Crypto');
var router 	= express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {  	
  	res.render('index', { title: 'Express' });
});	
router.get('/cp', function(req, res, next) {  	
  	res.render('change-password', { title: 'Express' });
});	


// router.get('/search', function(req, res, next) {  	
// 	var q = req.query.q;

//   	User.filters.search(q, function(err, resp){
//   		if( err ){
//   			return res.status(500).json(err);
//   		}

//   		return res.json(resp);
//   	});
// });	

// router.post('/cp', function(req, res, next) {  	
// 	console.log(req.body)
// 	var passwordPayload = req.body;

// 	if( passwordPayload.new == passwordPayload.newConfirm ){
// 		User.find( passwordPayload._id, function(err, user){

// 			if( user.password == Crypto.hash( passwordPayload.current ) ){
// 				User.update( passwordPayload._id, { password: passwordPayload.new }, function(err, updatedUser){
// 					return res.status(200).json( updatedUser );
// 				} );
// 				return;
// 			}
			
// 			return res.status(500).json({error: 'InvalidCurrentPass', msg: 'Current password doesn\'t match!'})
			
// 		} );

// 		return;
// 	}

// 	res.status(500).json({error: 'InvalidConfirmPass', msg: 'Password Confirmation doesn\'t match!'})

// });	

// router.get('/fetch-users', function(req, res, next) {	
// 	User.findAll(function(users){
//   		res.status(200).json(users);
// 	} );
// });	
module.exports = router;
